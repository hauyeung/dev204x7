﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dev204x7
{
    class Course
    {

        public ArrayList students;
        public Teacher teacher;
        public string name;
        public void ListStudents()
        {
            Console.WriteLine("Students in this course include:");
            foreach (object student in students)
            {                
                Console.WriteLine("{0} {1}", ((Student)student).firstname, ((Student)student).lastname);
                Console.WriteLine();
            }
        }
    }
}
